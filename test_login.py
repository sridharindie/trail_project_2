from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# URL of the Google website
url = "https://www.google.com/"

# Initialize Chrome WebDriver
chrome_op = webdriver.ChromeOptions()
chrome_op.add_argument("--headless")
driver = webdriver.Remote('http://localhost:5555',options = chrome_op)

try:
    # Open the Google website
    driver.get(url)

    # Find the search input field
    search_input = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.NAME, "q"))
    )

    # Type "Selenium" into the search input field
    search_input.send_keys("Selenium")

    # Press Enter to submit the search
    search_input.send_keys(Keys.RETURN)

    # Wait for the search results to load
    WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.ID, "search"))
    )

    # Get the search results
    search_results = driver.find_elements(By.CSS_SELECTOR, "#search .g")

    # Print the titles and URLs of the search results
    for result in search_results:
        title = result.find_element(By.CSS_SELECTOR, "h3").text
        url = result.find_element(By.CSS_SELECTOR, "a").get_attribute("href")
        print(title)
        print(url)
        print()

finally:
    # Close the browser window at the end of the test
    driver.quit()
